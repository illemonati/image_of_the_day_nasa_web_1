
import 'imotd_service.dart' as service;

class IMOTD{
  String imageUrl;
  String title;
  String description;
  String link;
  String pubDate;
  dynamic imageBase64;
  IMOTD(this.imageUrl, this.title, this.description, this.link, this.pubDate);

  void updateImageBase64() async {
    this.imageBase64 = await service.IMOTDService().getImageBase64(this.imageUrl);
    print('[Got Image] $imageBase64');
  }
}