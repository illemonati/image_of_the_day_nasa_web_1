import 'package:angular/angular.dart';
import 'imotd.dart';
import 'imotd_service.dart';
import 'package:angular_forms/angular_forms.dart';

@Component(
  selector: 'imotd-interface',
  templateUrl: 'imotd_interface.html',
  providers: [ClassProvider(IMOTDService)],
  styleUrls: ['package:angular_components/css/mdc_web/card/mdc-card.scss.css'],
  directives: [coreDirectives, formDirectives],
)
class IMOTDComponent implements OnInit{
  IMOTD imotd = new IMOTD("image_url", "title", "description", "link", "pubDate");
  bool rssReady = false;
  final IMOTDService _imotd_service;
  IMOTDComponent(this._imotd_service);
//  void ngOnInit() async {
//    this.imotd = await _imotd_service.getIMOTD();
//  }
  void ngOnInit() {
    _imotd_service.getIMOTD().then((imotd) {
      this.imotd = imotd;
      this.rssReady = true;
      this.imotd.updateImageBase64();
    });
  }
}