import 'package:webfeed/webfeed.dart';
import 'imotd.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' show base64;
import 'dart:typed_data' show Uint8List, ByteBuffer;

class IMOTDService {
  Future<IMOTD> getIMOTD() async {
    String url = 'https://cors-anywhere.herokuapp.com/www.nasa.gov/rss/dyn/lg_image_of_the_day.rss';
    var res = await http.get(url);
    var rawFeed = res.body;
    var feed = RssFeed.parse(rawFeed);
    var first = feed.items.first;
    var imotd = IMOTD(
        processUrl(first.enclosure.url), first.title, first.description, processUrl(first.link),
        first.pubDate);
    return imotd;
  }
  String processUrl(String ogUrl) {
    if (ogUrl.substring(0, 5) == 'http:') {
      return 'https:' + ogUrl.substring(4, ogUrl.length);
    }
    return ogUrl;
  }

  String processUrlForCORS(String ogUrl) {
    RegExp re = new RegExp(r"(http[s]?:\/\/)(.*)");
    String s = 'https://www.nasa.gov/sites/default/files/thumbnails/image/40659647513_000ebd78f2_k.jpg';
    String url1 = re.firstMatch(s).group(2);
    return 'https://cors-anywhere.herokuapp.com/' + url1;
  }

  Future<String> getImageBase64(String url) async {
    var res = await http.get(processUrlForCORS(processUrl(url)));
    var imageList = res.bodyBytes;
    String contentType = res.headers['content-type'];
    String header = 'data:$contentType;base64,';
    var imageBase64 = base64.encode(imageList);
    String image = '$header$imageBase64';
    return image;
  }

}