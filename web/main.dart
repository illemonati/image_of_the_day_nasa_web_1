import 'package:angular/angular.dart';
import 'package:image_of_the_day_nasa_web_1/app_component.template.dart' as ng;

void main() {
  runApp(ng.AppComponentNgFactory);
}
